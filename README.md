# ETH RDM Summer School 2019 - Workshop on ARDM and Reproducibility

This is the repository for the ETH RDM Summer School Workshop on ARDM and Reproducibility.

Instructors:
- Dr. Caterina Barillari
- Dr. Henry Lütcke

The URL of the openBIS test server for this tutorial is:
https://openbis-tst.ethz.ch/openbis/webapp/eln-lims/

The URL of the Jupyter test server for this tutorial is:
https://jupyterhub-tst.ethz.ch

On both servers, you can login with your NETHZ credentials (students from outside ETH use the guest accounts that have been provided).

