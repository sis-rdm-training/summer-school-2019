# ETH RDM Summer School 2019 - Workshop on Jupyter notebooks

Instructor:
- Dr. Henry Lütcke

## Prerequisites
- Clone / download the git repository with files and instructions to your computer: https://gitlab.ethz.ch/sis-rdm-training/summer-school-2019
- The URL of the Jupyter test server for this tutorial is https://jupyterhub-tst.ethz.ch
- Login with your NETHZ credentials (or guest account)
- On the Jupyter Dashboard, upload the following files to the server (from the Jupyter folder of the repository):
    1. Notebook Basics.ipynb
    2. Beyond Plain Python.ipynb
    3. Jupyter_Image_Analysis_Notebook.ipynb
- Next, create a new folder by selecting `New` and then `Folder`
    - Rename the folder to `data`
    - Enter the `data` folder and upload the file `blobs.tif` from you computer
    - Go back to the home folder on the Jupyter server
- **Note: access to the Jupyter server will be removed after the summer school. You can however work through the tutorial on a public Jupyter server, such as https://jupyter.org/try or install Jupyter on your own machine.**

## Jupyter basics
The notebook file `Notebook Basics.ipynb` explains some basics about the Jupyter interface and working with notebooks. Work through the notebook either together with the instructor or at your own speed.

## Beyond the basics
The notebook file `Beyond Plain Python.ipynb` explains some more advanced tricks when working with Jupyter notebooks and [IPython](https://ipython.org/). It also shows some Python coding examples and how to plot with `matplotlib`. Work through the notebook at your own speed. Change and explore as you go along. 
Hint: Documentation cells are written in `Markdown` syntax. A handy markdown cheat-sheet is available here: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

## Simple image analysis with Jupyter and Python
If you have not done so, close the two open notebooks (`File` --> `Close and Halt`). Now we will see how to do some actual data analysis with Jupyter notebooks and Python. 

Open the notebook file `Jupyter_Image_Analysis_Notebook.ipynb`. In this notebook, we will analyze the `blobs.tif` image in the `data` folder. Work through the notebook either together with the instructor or at your own speed. Change and / or explore code, parameters, results and documentation as you go along.

## Jupyter - openBIS integration
A Jupyter notebook can be launched directly from the openBIS ELN user interface. Currently, a bug requires that we have to connect manually to openBIS from the notebook. To do this, add the following code to the cell that connects to openBIS:
```python
from pybis import Openbis
o = Openbis()

import getpass
password = getpass.getpass()

o.login('username', password, save_token=True)
```
This will be fixed in a future release.


